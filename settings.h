#pragma once

#include <string_view>
#include <httplib/httplib.h>

struct UserSettings {
    bool auto_open_cw = false;

    UserSettings() = default;
    UserSettings(const httplib::Request& req) {
        this->load_from_cookies(req);
    }

    void set(std::string_view key, std::string_view value);
    void load_from_cookies(const httplib::Request& req);
};
