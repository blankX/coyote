#pragma once

#include <string>
#include <vector>
#include <memory>
#include <optional>
#include <nlohmann/json_fwd.hpp>

#include "blankie/serializer.h"

enum PostSortingMethod {
    Posts = 0,
    PostsAndReplies,
    MediaOnly,
};

struct Emoji {
    std::string shortcode;
    std::string url;
};

struct AccountField {
    std::string name;
    blankie::html::HTMLString value_html;
    time_t verified_at; // negative is not verified
};
struct Account {
    std::string id;
    std::string username;
    std::string server;
    bool same_server;
    std::string display_name;
    bool bot;
    time_t created_at;
    blankie::html::HTMLString note_html;
    std::string avatar;
    std::string avatar_static;
    std::string header;
    int64_t followers_count;
    uint64_t following_count;
    uint64_t statuses_count;
    std::vector<Emoji> emojis;
    std::vector<AccountField> fields;

    inline std::string acct(bool always_show_domain = true) const {
        std::string res = this->username;
        if (always_show_domain || !this->same_server) {
            res += '@';
            res += this->server;
        }
        return res;
    }
};

struct Size {
    uint64_t width;
    uint64_t height;
};
struct Media {
    std::string type;
    std::string url;
    std::optional<std::string> preview_url;
    std::optional<std::string> remote_url;
    std::optional<Size> size;
    std::optional<Size> preview_size;
    std::optional<std::string> description;
};

struct PollOption {
    std::string title;
    uint64_t votes_count;
};
struct Poll {
    time_t expires_at; // negative if none
    bool expired;
    int64_t voters_count; // negative if unknown
    uint64_t votes_count;
    std::vector<PollOption> options;
    std::vector<Emoji> emojis;
};

struct Post {
    std::string id;
    time_t created_at;
    std::optional<std::string> in_reply_to_id;
    std::optional<std::string> in_reply_to_account_id;
    bool sensitive;
    std::string spoiler_text;
    uint64_t replies_count;
    uint64_t reblogs_count;
    uint64_t favorites_count;
    time_t edited_at; // negative is not edited
    blankie::html::HTMLString content;
    std::unique_ptr<Post> reblog;
    Account account;
    std::vector<Media> media_attachments;
    std::vector<Emoji> emojis;
    std::optional<Poll> poll;
};

struct PostContext {
    std::vector<Post> ancestors;
    std::vector<Post> descendants;
};

struct Instance {
    std::string title;
    std::string description;
    std::string thumbnail;
    std::string contact_email;
    Account contact_account;
    std::vector<std::string> rules;
};

void from_json(const nlohmann::json& j, Emoji& emoji);
void from_json(const nlohmann::json& j, AccountField& field);
void from_json(const nlohmann::json& j, Account& account);
void from_json(const nlohmann::json& j, Size& size);
void from_json(const nlohmann::json& j, Media& media);
void from_json(const nlohmann::json& j, PollOption& option);
void from_json(const nlohmann::json& j, Poll& poll);
void from_json(const nlohmann::json& j, Post& post);
void from_json(const nlohmann::json& j, PostContext& context);
void from_json(const nlohmann::json& j, Instance& instance);
