#include <string>
#include <stdexcept>
#include "settings.h"
#include "servehelper.h"

static bool parse_bool(std::string_view value);


void UserSettings::set(std::string_view key, std::string_view value) {
    if (key == "auto-open-cw") {
        this->auto_open_cw = parse_bool(value);
    }
}

void UserSettings::load_from_cookies(const httplib::Request& req) {
    Cookies cookies = parse_cookies(req);

    for (auto &[name, value] : cookies) {
        this->set(name, value);
    }
}


static bool parse_bool(std::string_view value) {
    using namespace std::string_literals;

    if (value == "true") {
        return true;
    } else if (value == "false") {
        return false;
    } else {
        throw std::invalid_argument("unknown boolean value: "s + std::string(value));
    }
}
