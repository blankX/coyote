Coyote, a Javascript-less Mastodon frontend. Currently supports viewing a user,
their posts, posts themselves (and their context), posts with a certain
hashtag, and instance details.

The source code to Coyote is available under the MIT license. Some icons are
used from [Font Awesome] Free, which are created by Fonticons, Inc. and
[licensed] under the [CC-BY 4.0]. Icons used are slightly modified for better
accessibility.

[Font Awesome]: https://fontawesome.com/
[licensed]: https://fontawesome.com/license/free#icons
[CC-BY 4.0]: https://creativecommons.org/licenses/by/4.0/
