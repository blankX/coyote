#include "routes.h"
#include "../servehelper.h"
#include "../htmlhelper.h"
#include "../client.h"
#include "../models.h"

void tags_route(const httplib::Request& req, httplib::Response& res) {
    using namespace std::string_literals;

    std::string server = req.matches.str(1);
    std::string tag = req.matches.str(2);
    std::optional<std::string> max_id;
    if (req.has_param("max_id")) {
        max_id = req.get_param_value("max_id");
    }

    std::vector<Post> posts;
    try {
        posts = mastodon_client.get_tag_timeline(server, tag, max_id);
    } catch (const std::exception& e) {
        res.status = 500;
        serve_error(req, res, "500: Internal server error", "Failed to fetch tag timeline", e.what());
        return;
    }

    Element body("body", {Element("h2", {"Posts for #", tag})});
    if (!posts.empty()) {
        body.nodes.reserve(body.nodes.size() + posts.size() * 2 - 1 + 2);

        for (size_t i = 0; i < posts.size(); i++) {
            if (i != 0) {
                body.nodes.push_back(Element("hr"));
            }
            body.nodes.push_back(serialize_post(req, server, posts[i]));
        }

        body.nodes.push_back(Element("hr"));
        body.nodes.push_back(Element("a", {{"class", "more_posts"}, {"href", "?max_id="s + posts[posts.size() - 1].id}}, {"See more"}));
    } else {
        body.nodes.push_back(Element("p", {{"class", "more_posts"}}, {
            max_id ? "There are no more posts" : "No results found",
        }));
    }

    serve(req, res, "Posts for #"s + tag, std::move(body));
}
