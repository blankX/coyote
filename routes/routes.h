#pragma once

#include <httplib/httplib.h>

extern const uint64_t css_hash;

void home_route(const httplib::Request& req, httplib::Response& res);
void css_route(const httplib::Request& req, httplib::Response& res);
void user_route(const httplib::Request& req, httplib::Response& res);
void status_route(const httplib::Request& req, httplib::Response& res);
void tags_route(const httplib::Request& req, httplib::Response& res);
void about_route(const httplib::Request& req, httplib::Response& res);
void user_settings_route(const httplib::Request& req, httplib::Response& res);
