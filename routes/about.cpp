#include "routes.h"
#include "../servehelper.h"
#include "../htmlhelper.h"
#include "../client.h"
#include "../models.h"

void about_route(const httplib::Request& req, httplib::Response& res) {
    using namespace std::string_literals;

    std::string server = req.matches.str(1);

    Instance instance;
    blankie::html::HTMLString extended_description;
    try {
        instance = mastodon_client.get_instance(server);
        extended_description = mastodon_client.get_extended_description(server);
    } catch (const std::exception& e) {
        res.status = 500;
        serve_error(req, res, "500: Internal server error", "Failed to fetch instance information", e.what());
        return;
    }

    Element rules("ol");
    rules.nodes.reserve(instance.rules.size());
    for (const std::string& rule : instance.rules) {
        rules.nodes.push_back(Element("li", {rule}));
    }

    Element body("body", {
        Element("a", {{"href", instance.thumbnail}}, {
            Element("img", {{"class", "about_page-header"}, {"loading", "lazy"}, {"src", instance.thumbnail}}, {}),
        }),
        Element("p", {
            Element("b", {instance.title, ":"}), " ", instance.description,
            Element("br"), Element("b", {"Email:"}), " ", Element("a", {{"href", "mailto:"s + instance.contact_email}}, {instance.contact_email}),
            Element("br"), Element("b", {"Administered by:"}), " ", Element("a", {{"href", get_origin(req) + '/' + server + "/@" + instance.contact_account.acct(false)}}, {
                preprocess_html(req, instance.contact_account.emojis, instance.contact_account.display_name), " (@", instance.contact_account.acct(false), ")",
            }),
        }),

        !extended_description.str.empty() ? Element("details", {{"class", "about_page-about"}, {"open", ""}}, {
            Element("summary", {"About"}),
            preprocess_html(req, server, {}, std::move(extended_description)),
        }) : Node(""),

        Element("details", {{"class", "about_page-rules"}}, {
            Element("summary", {"Rules"}),
            rules,
        }),
    });
    serve(req, res, "About "s + instance.title, std::move(body));
}
