#include <string>
#include <FastHash.h>

#include "routes.h"
#include "../servehelper.h"

static const constexpr char css[] = R"EOF(
:root {
    --bg-color: black;
    --fg-color: white;
    --font-size: 13pt;

    --error-background-color: rgb(100, 0, 0);
    --error-border-color: red;
    --error-text-color: white;

    --success-background-color: green;
    --success-border-color: lightgreen;
    --success-text-color: white;

    --accent-color: #962AC3;
    --bright-accent-color: #DE6DE6;
}

* {
    margin: 0;
    padding: 0;
}
/* .quote-inline and display: block: workaround for vt.social/@lina?max_id=111463476293964258 */
p, details, .quote-inline {
    margin-top: 1em;
    margin-bottom: 1em;
    display: block;
}
ul, ol {
    margin: revert;
    padding: revert;
}
details[open] {
    margin-bottom: 0;
}
summary:hover {
    text-decoration: underline;
    cursor: pointer;
}

html {
    background-color: var(--bg-color);
    color: var(--fg-color);
    font-size: var(--font-size);
    font-family: sans-serif;
    padding: 10px;
    /* https://queer.party/@cafkafk@akko.wtf/112087113708496854 */
    overflow-wrap: anywhere;
}
pre {
    /* https://stackoverflow.com/a/248013 */
    white-space: pre-wrap;
}

img {
    object-fit: cover;
    /* about page for lgbtqia.space */
    max-width: 100%;
}

a {
    color: var(--accent-color);
}
a:hover {
    color: var(--bright-accent-color);
}

/* CUSTOM EMOJI and FONT AWESOME ICONS */
.custom_emoji, svg {
    height: 1em;
    width: 1em;
    /* https://stackoverflow.com/a/489394 */
    vertical-align: middle;
}
/* FONT AWESOME ICONS */
svg {
    filter: invert(1);
}

/* POST */
.main_post .post-contents, .main_post details {
    font-size: 1.1rem;
}
.post {
    margin-top: 1em;
}
.post-header, .post-header a {
    display: flex;
}
.post-header a {
    color: inherit;
    text-decoration: none;
}
.post-header a:hover {
    text-decoration: underline;
}
.post-avatar {
    width: 3em;
    height: 3em;
    margin-right: 0.5em;
}
.post-header span, .post-header time {
    margin-top: auto;
    margin-bottom: auto;
}
.post-header time {
    padding-left: 1em;
    text-align: right;
}
.post-time_header {
    margin-left: auto;
}

.post-attachments:not(:empty) {
    margin-top: 1em;
    margin-bottom: 1em;
}
/* https://stackoverflow.com/a/7354648 */
@media (min-width: 801px) {
    .post-attachments {
        display: grid;
        grid-template-columns: 1fr 1fr;
        grid-gap: 0.5em;
        width: fit-content;
    }

    .post-attachments * {
        margin: initial;
    }
}
.post-attachments img, .post-attachments video, .post-attachments audio {
    width: 320px;
    height: 180px;
}
/* do not ask why not fit-content or min-content. i spent one hour on this, and that is one hour too long to be spending on this god damn grid */
.post-attachments audio {
    height: 40px;
}
/* https://chaosfem.tw/@betsystreeter@mastodon.art/111456606953658581 */
.post-attachments .unknown_media {
    max-width: 320px;
    max-height: 180px;
}

/* POLL */
.poll-option {
    margin-top: 0.5em;
    margin-bottom: 0.5em;
}
.poll-percentage {
    margin-right: 0.5em;
}
.poll-bar {
    height: 0.5em;
    display: block;
    background-color: lightgray;
    border-radius: 10em;
    margin-top: 0.25em;
}
.poll-bar[width="0%"] {
    width: 0.5em;
}

/* ERROR PAGE */
.error {
    text-align: center;
    background-color: var(--error-background-color);
    color: var(--error-text-color);
    border-style: solid;
    border-color: var(--error-border-color);
}
.error * {
    margin: revert;
    padding: revert;
}

/* SUCCESS BOX */
.success {
    text-align: center;
    background-color: var(--success-background-color);
    color: var(--success-text-color);
    border-style: solid;
    border-color: var(--success-border-color);
    margin-bottom: 0.5em;
}
.success * {
    margin: revert;
    padding: revert;
}

/* USER PAGE */
.user_page-header {
    width: 100%;
    height: 15em;
}

.user_page-main_header {
    display: flex;
}
.user_page-profile {
    width: 7.5em;
    height: 7.5em;
    min-width: 7.5em;
    min-height: 7.5em;
}
.user_page-main_header_text {
    margin-top: auto;
    margin-bottom: auto;
    margin-left: 0.5em;
}

/* https://stackoverflow.com/a/7354648 */
@media (min-width: 801px) {
    .user_page-user_description {
        display: grid;
        grid-template-columns: 1fr 1fr;
        grid-gap: 1em;
    }
}

.user_page-user_bio {
    line-height: 1.25;
}

.user_page-user_links {
    height: fit-content;
    width: fit-content;
    text-align: left;
    margin-top: 1em;
    margin-bottom: 1em;
    /* https://stackoverflow.com/a/5680823 */
    border-collapse: collapse;
}
.user_page-user_links td {
    padding-left: 1em;
}
.user_page-user_links .verified {
    background-color: rgba(0, 150, 0, 0.3);
}
.user_page-user_links .verified td::after {
    content: " ✓";
    color: rgb(100, 255, 100);
}

.user_page-user_posts_nav {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    text-align: center;
}
.user_page-user_posts_nav a {
    text-decoration: none;
}
.user_page-user_posts_nav .selected {
    font-weight: bold;
    color: inherit;
}
.user_page-user_posts_nav .selected:hover {
    text-decoration: underline;
}

/* USER PAGE and TAGS PAGE */
.more_posts {
    margin-top: 1em;
    text-align: center;
    /* don't ask why, but making it a block element just works */
    display: block;
}

/* ABOUT PAGE */
.about_page-header {
    width: 100%;
    height: 15em;
}
.about_page-about > summary, .about_page-rules > summary {
    font-size: 130%;
    font-weight: bold;
}
.about_page-about * {
    margin: revert;
    padding: revert;
}

/* USER SETTINGS PAGE */
.user_settings_page-form {
    display: inline;
}
.user_settings_page-form input[type=submit] {
    margin-top: 0.5em;
    padding: revert;
}
.user_settings_page-form .cancel {
    margin-left: 0.25em;
}
)EOF";
// one for \0, one for trailing newline
#define CSS_LEN sizeof(css) / sizeof(css[0]) - 2

#if __cpp_constinit
    #define CONSTINIT constinit
#else
    #define CONSTINIT
#endif

CONSTINIT const uint64_t css_hash = FastHash(css, CSS_LEN, 0);

void css_route(const httplib::Request& req, httplib::Response& res) {
    using namespace std::string_literals;

    res.set_header("ETag", "\""s + std::to_string(css_hash) + '"');
    res.set_header("Cache-Control", "max-age=31536000, immutable");

    if (should_send_304(req, css_hash)) {
        res.status = 304;
        res.set_header("Content-Length", std::to_string(CSS_LEN));
        res.set_header("Content-Type", "text/css");
    } else {
        res.set_content(css, CSS_LEN, "text/css");
    }
}
