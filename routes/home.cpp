#include "routes.h"
#include "../servehelper.h"

void home_route(const httplib::Request& req, httplib::Response& res) {
    Element body("body", {
        Element("p", {
            "Coyote, a Javascript-less Mastodon frontend. Currently supports viewing a user, "
            "their posts, posts themselves (and their context), posts with a certain hashtag, "
            "and instance details.",
        }),
        Element("p", {Element("a", {{"href", get_origin(req) + "/settings"}}, {"Configure settings"})}),
        Element("h2", {"Example links"}),
        Element("ul", {
            Element("li", {Element("a", {{"href", get_origin(req) + "/vt.social/@lina"}}, {"Asahi Lina (朝日リナ) // nullptr::live"})}),
            Element("li", {Element("a", {{"href", get_origin(req) + "/vt.social/@LucydiaLuminous/111290028216105435"}}, {"\"I love kids and their creativity. So I was explain…\""})}),
            Element("li", {Element("a", {{"href", get_origin(req) + "/ruby.social/@CoralineAda/109951421922797743"}}, {"\"My partner just said \"I'm starting to think nostal…\""})}),
            Element("li", {Element("a", {{"href", get_origin(req) + "/pawoo.net/tags/OMORIFANART"}}, {"#OMORIFANART"})}),
        }),
        Element("hr"),
        Element("p", {
            "The ", Element("a", {{"href", "https://gitlab.com/blankX/coyote"}}, {"source code"}), " to Coyote is available under the ",
            Element("a", {{"href", "https://gitlab.com/blankX/coyote/-/blob/master/LICENSE"}}, {"MIT license"}), ". "
            "Some icons are used from ", Element("a", {{"href", "https://fontawesome.com"}}, {"Font Awesome"}), " Free, which are created by Fonticons, Inc. and ",
            Element("a", {{"href", "https://fontawesome.com/license/free#icons"}}, {"licensed"}), " under the ",
            Element("a", {{"href", "https://creativecommons.org/licenses/by/4.0/"}}, {"CC-BY 4.0"}), ". "
            "Icons used are slightly modified for better accessibility."
        }),
    });

    serve(req, res, "Coyote", std::move(body));
}
