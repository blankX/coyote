#pragma once

#include <string>
#include <cstdlib>

unsigned long long to_ull(const std::string& str);
int to_int(const std::string& str);
