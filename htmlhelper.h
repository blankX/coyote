#pragma once

#include "lxb_wrapper.h"

#include "servehelper.h"
struct Post; // forward declaration from models.h
struct Emoji; // forward declaration from models.h

Element serialize_post(const httplib::Request& req, const std::string& server, const Post& post, bool pinned = false, bool main_post = false);

std::string get_text_content(lxb_dom_node_t* child);
std::string get_text_content(blankie::html::HTMLString str);
blankie::html::HTMLString preprocess_html(const httplib::Request& req, const std::string& domain_name, const std::vector<Emoji>& emojis, const blankie::html::HTMLString& str);
blankie::html::HTMLString preprocess_html(const httplib::Request& req, const std::vector<Emoji>& emojis, const std::string& str);
