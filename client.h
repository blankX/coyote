#pragma once

#include <mutex>
#include <optional>
#include <exception>
#include <pthread.h>
#include <curl/curl.h>
#include <nlohmann/json.hpp>

#include "models.h"
class CurlUrl; // forward declaration from curlu_wrapper.h

class CurlException : public std::exception {
public:
    CurlException(CURLcode code_) : code(code_) {}

    const char* what() const noexcept {
        return curl_easy_strerror(this->code);
    }

    CURLcode code;
};

class CurlShareException : public std::exception {
public:
    CurlShareException(CURLSHcode code_) : code(code_) {}

    const char* what() const noexcept {
        return curl_share_strerror(this->code);
    }

    CURLSHcode code;
};

class MastodonException : public std::exception {
public:
    MastodonException(long response_code_, std::string error) : response_code(response_code_), _error(std::move(error)) {}

    const char* what() const noexcept {
        return this->_error.c_str();
    }

    long response_code;

private:
    std::string _error;
};

class MastodonClient {
public:
    MastodonClient(const MastodonClient&&) = delete;
    MastodonClient& operator=(const MastodonClient&&) = delete;

    MastodonClient();
    ~MastodonClient();

    static void init() {
        CURLcode code = curl_global_init(CURL_GLOBAL_ALL);
        if (code) {
            throw CurlException(code);
        }
    }

    static void cleanup() {
        curl_global_cleanup();
    }

    std::optional<Account> get_account_by_username(std::string host, std::string username);
    std::vector<Post> get_pinned_posts(std::string host, const std::string& account_id);
    std::vector<Post> get_posts(const std::string& host, const std::string& account_id, PostSortingMethod sorting_method, std::optional<std::string> max_id);

    std::optional<Post> get_post(const std::string& host, std::string id);
    PostContext get_post_context(const std::string& host, std::string id);

    std::vector<Post> get_tag_timeline(const std::string& host, const std::string& tag, std::optional<std::string> max_id);

    Instance get_instance(std::string host);
    blankie::html::HTMLString get_extended_description(std::string host);

private:
    CURL* _get_easy();
    nlohmann::json _send_request(std::optional<std::string> cache_key, const CurlUrl& url);
    long _response_status_code();

    std::mutex _share_locks[CURL_LOCK_DATA_LAST];

    CURLSH* _share = nullptr;
    pthread_key_t _easy_key;
};

extern MastodonClient mastodon_client;
