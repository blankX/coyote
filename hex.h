#pragma once

#include <string>
#include <vector>

std::string hex_encode(const char* in, size_t in_size);
inline std::string hex_encode(const std::vector<char>& in) {
    return hex_encode(in.data(), in.size());
}
std::string percent_encode(std::string_view in);

std::vector<char> hex_decode(std::string_view in);
