#pragma once

#include <array>
#include <vector>
#include <exception>

#include <openssl/err.h>

class OpenSSLException : public std::exception {
public:
    OpenSSLException(unsigned long e) {
        ERR_error_string_n(e, this->_str, 1024);
    }

    const char* what() const noexcept {
        return this->_str;
    }

private:
    char _str[1024];
};

std::vector<char> secure_random_bytes(int num);
std::array<char, 32> hmac_sha3_256(const std::vector<char>& key, const std::vector<char>& data);
