#pragma once

#include <ctime>
#include <string>

time_t current_time();

std::string short_time(time_t time);
std::string full_time(time_t time);
std::string to_rfc3339(time_t time);
std::string to_web_date(time_t time);
std::string relative_time(time_t from, time_t to);
