#pragma once

#include <string>
#include <vector>
#include <variant>
#include <utility>

namespace blankie {
namespace html {

struct Element;
struct HTMLString {
    HTMLString() = default;
    explicit HTMLString(std::string str_) : str(std::move(str_)) {}
    HTMLString& operator=(std::string str_) {
        this->str = std::move(str_);
        return *this;
    }

    std::string str;
};

typedef std::pair<const char*, std::string> Attribute;
typedef std::variant<Element, const char*, std::string, HTMLString> Node;

struct Element {
    const char* tag;
    std::vector<Attribute> attributes;
    std::vector<Node> nodes;

    Element(const char* tag_) : tag(tag_) {}
    Element(const char* tag_, std::vector<Node> nodes_)
        : tag(tag_), nodes(std::move(nodes_)) {}
    Element(const char* tag_, std::vector<Attribute> attributes_, std::vector<Node> nodes_)
        : tag(tag_), attributes(std::move(attributes_)), nodes(std::move(nodes_)) {}
    std::string serialize() const;
};

} // namespace html
} // namespace blankie
