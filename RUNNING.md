## Compiling

- `mkdir build`
- `cd build`
- Execute one of the following commands depending on your conditions:
  - If this is going to be on production (i.e. not for debugging),
    run `cmake -B . .. -DCMAKE_BUILD_TYPE=Release`
  - If this is for debugging, run `cmake -B . .. -DCMAKE_BUILD_TYPE=Debug`
- `make -j$(nproc)`

## Configuration

Copy `example_config.json` to a file with any name you like
(e.g. `config.json`), then open the copied file and edit the parameters to your
liking. Here's a list of what they are:
- `bind_host` (string): What address to bind to
- `bind_port` (zero or positive integer): What port to bind to
- `hmac_key` (hex string): A secret key to be used; generate with `head -c32 /dev/urandom | basenc --base16`
- `canonical_origin` (string or null): A fallback canonical origin if set, useful if you're, say, running Coyote behind Ngrok
- `redis` (object)
  - `enabled` (boolean)
  - `connection` (object)
    - `type` ("ip" or "unix"): Whether to connect to redis via TCP or UNIX domain sockets
    - `address` (string or null): If `type` is "ip", the address to connect to. Ignored otherwise
    - `port` (int or null): If `type` is "ip", the port to connect to. Ignored otherwise
    - `unix` (string or null): If `type` is "unix", the path to redis' socket. Ignored otherwise
  - `username` (string or null): Optional username for authentication
  - `password` (string or null): Optional password for authentication

Coyote is intended to be run behind a reverse proxy (e.g. Nginx), so you should
set your reverse proxy to proxy requests to
`http://localhost:<port in config file>`, and set a `X-Canonical-Origin` header
for all requests coming through to the URL for your instance. For example, if
your instance is at https://coyote.example.com, you should set the header to
`https://coyote.example.com`.

## Running

Run `./path/to/repository/build/coyote` with the path to your copied config
file as an argument
