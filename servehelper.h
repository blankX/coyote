#pragma once

#include <optional>
#include <unordered_map>

#include <httplib/httplib.h>
#include "blankie/serializer.h"
class CurlUrl; // forward declaration from curlu_wrapper.h

using Element = blankie::html::Element;
using Node = blankie::html::Node;
using Nodes = std::vector<Node>;
using Cookies = std::unordered_map<std::string, std::string>;

void serve(const httplib::Request& req, httplib::Response& res, std::string title, Element element, Nodes extra_head = {});
void serve_error(const httplib::Request& req, httplib::Response& res,
        std::string title, std::optional<std::string> subtitle = std::nullopt, std::optional<std::string> info = std::nullopt);
void serve_redirect(const httplib::Request& req, httplib::Response& res, std::string url, bool permanent = false);

bool starts_with(const CurlUrl& url, const CurlUrl& base);
std::string get_origin(const httplib::Request& req);
std::string proxy_mastodon_url(const httplib::Request& req, const std::string& url_str);
bool should_send_304(const httplib::Request& req, uint64_t hash);

Cookies parse_cookies(const httplib::Request& req);
